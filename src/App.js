import React from "react";
import "./App.scss";
import MainRouter from "./components/router";

function App() {
  return (
    <div className="App">
      <MainRouter />
    </div>
  );
}

export default App;
