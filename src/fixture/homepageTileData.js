const tileData = [
  {
    img: "/images/default_game_image.jpg",
    title: "Tankathon",
    author: "Ngoc",
    cols: 2,
    featured: true,
    route: "tankathon",
  },
  {
    img: "/images/default_game_image.jpg",
    title: "Tanks",
    author: "Ngoc",
    route: "homepage",
  },
];

export default tileData;
