import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Homepage from "../views/Homepage";
import TopNavigation from "../common/TopNavigation";
import DocumentMeta from "react-document-meta";
import NotFound from "../common/NotFound";
import PropTypes from "prop-types";
import Link from "@material-ui/core/Link";
import Tankathon from "../views/Tankathon/Tankathon";

const baseMetaData = {
  title: "Life Adventurers - Mini Games",
  description: "Life Adventurers - Mini Games : Small games for fun",
  meta: {
    charSet: "utf-8",
    name: {
      keywords: "react,life adventurers,games,mini games",
    },
  },
};

export const getRoutesConfig = () => [
  {
    name: "homepage",
    exact: true,
    path: "/",
    meta: {
      ...baseMetaData,
      title: "Homepage",
    },
    label: "Home",
    component: Homepage,
  },
  {
    name: "tankathon",
    exact: true,
    path: "/tankathon",
    meta: {
      ...baseMetaData,
      title: "Tankathon",
    },
    label: "Tankathon",
    component: Tankathon,
  },
];

export const findRoute = (to) => getRoutesConfig().find((rt) => rt.name === to);

export const NamedLink = ({ className, to, children, ...props }) => {
  const route = findRoute(to);
  if (!route) throw new Error(`Route to '${to}' not found`);
  return (
    <Route
      path={route.path}
      exact
      children={(
        { match } // eslint-disable-line react/no-children-prop
      ) => (
        <Link href={route.path} {...props} className={className}>
          {children || route.label}
        </Link>
      )}
    />
  );
};

NamedLink.propTypes = {
  to: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.element,
};

NamedLink.defaultProps = {
  className: "",
  children: null,
};

const RouteWithMeta = ({ component: Component, meta, ...props }) => (
  <Route
    {...props}
    render={(matchProps) => (
      <span>
        <DocumentMeta {...meta} />
        <Component {...matchProps} />
      </span>
    )}
  />
);

RouteWithMeta.propTypes = {
  component: PropTypes.func.isRequired,
  meta: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    meta: PropTypes.shape({
      charSet: PropTypes.string,
      name: PropTypes.shape({
        keywords: PropTypes.string,
      }),
    }),
  }).isRequired,
};

export default function MainRouter() {
  return (
    <Router>
      <div>
        <TopNavigation />
        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
          {getRoutesConfig().map((route) => (
            <RouteWithMeta {...route} key={route.name} />
          ))}
          <Route title="Page Not Found - React Lego" component={NotFound} />
        </Switch>
      </div>
    </Router>
  );
}
