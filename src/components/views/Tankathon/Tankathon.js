import React, { Component } from "react";
import * as PIXI from "pixi.js";
import { Timer, TimerManager } from "eventemitter3-timer";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";

const PLAYER_STATES = {
  JUMPING: 1,
  FALLING: 2,
  NORMAL: 0,
  DIED: -1,
};

class Tankathon extends Component {
  constructor(props) {
    super(props);
    this.speedIncrement = 1.1;
    this.bgX = 0;
    this.bgSpeed = 1;
    this.viewWidth = 800;
    this.viewHeight = 600;
    this.app = null;
    this.bgBack = null;
    this.player = null;
    this.playerBaseStats = {
      width: 40,
      height: 40,
      baseX: 20,
      baseY: 0,
      boundaryY: 0,
      xVelocity: 0,
      yVelocity: 0,
      baseVelocity: 3,
      state: PLAYER_STATES.NORMAL,
    };
    this.enemies = [];
    this.enemyBaseStats = {
      width: 80,
      height: 80,
      baseX: 20,
      baseY: 0,
      boundaryY: 0,
      xVelocity: 0,
      yVelocity: 0,
      baseVelocity: 5,
      state: PLAYER_STATES.NORMAL,
      generateMaxSpeed: 6000,
      generateMinSpeed: 3000,
      generateMaxSpeedLimit: 3000,
      generateMinSpeedLimit: 1500,
    };
    this.collisionGap = 20;
    this.score = 0;
    this.pixiScore = null;
    this.state = {
      open: false,
      message: "",
      severity: "success",
    };
    this.handleCloseSnackbar = this.handleCloseSnackbar.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleReload = this.handleReload.bind(this);
  }
  componentDidMount() {
    this.initPixiApp();
    this.gameZone.appendChild(this.app.view);
  }
  initPixiApp() {
    this.app = new PIXI.Application({
      backgroundColor: 0xaaaaaa,
      width: this.viewWidth,
      height: this.viewHeight,
      sharedLoader: true,
      sharedTicker: true,
    });
    this.app.loader.baseUrl = "/images/";
    this.app.loader.add("bgBack", "tankathon_background.jpg");
    this.app.loader.add("tank", "tank.png");
    this.app.loader.add("tankReverse", "tank_reverse.png");
    this.app.loader.onComplete.add(() => {
      this.initLevel();
    });
    this.app.loader.load();
  }
  initLevel() {
    this.bgBack = this.createBg(this.app.loader.resources["bgBack"].texture);
    this.pixiScore = this.createScore();
    this.enemyBaseStats.baseY = this.app.view.height - 105;
    this.enemyBaseStats.baseX = this.app.view.width - 20;
    // create generate enemy timer
    let generateEnemyTimer = new Timer(2000); // in ms
    generateEnemyTimer.loop = true;
    generateEnemyTimer.on("repeat", () => {
      this.enemies.push(
        this.createObject(
          this.app.loader.resources["tankReverse"].texture,
          this.enemyBaseStats.width,
          this.enemyBaseStats.height,
          this.enemyBaseStats.baseX,
          this.enemyBaseStats.baseY
        )
      );
      generateEnemyTimer.reset(); // Reset the generateEnemyTimer
      generateEnemyTimer.time = this.generateRandomInt(
        this.enemyBaseStats.generateMaxSpeed,
        this.enemyBaseStats.generateMinSpeed
      ); // Set new time randomly
      generateEnemyTimer.start(); // And start again
    });
    generateEnemyTimer.start();

    // create increase speed timer
    let increaseSpeedTimer = new Timer(10000); // in ms
    increaseSpeedTimer.loop = true;
    increaseSpeedTimer.on("repeat", () => this.increaseGameSpeed());
    increaseSpeedTimer.start();

    this.app.ticker.add(() => {
      this.gameLoop();
      generateEnemyTimer.timerManager.update(this.app.ticker.elapsedMS);
      increaseSpeedTimer.timerManager.update(this.app.ticker.elapsedMS);
    });

    // add player
    this.playerBaseStats.baseY = this.app.view.height - 70;
    this.playerBaseStats.boundaryY = this.app.view.height - 170;
    this.player = this.createObject(
      this.app.loader.resources["tank"].texture,
      this.playerBaseStats.width,
      this.playerBaseStats.height,
      this.playerBaseStats.baseX,
      this.playerBaseStats.baseY
    );

    let space = this.keyboard(" ");
    // jumping event
    space.press = () => {
      if (this.playerBaseStats.state === PLAYER_STATES.NORMAL) {
        this.playerBaseStats.state = PLAYER_STATES.JUMPING;
      }
    };
  }
  increaseGameSpeed() {
    if (this.playerBaseStats.state === PLAYER_STATES.DIED) {
      return;
    }
    this.setState({
      open: true,
      message: "Speed increased",
      severity: "success",
    });
    console.log("snackbar", this.snackbar);
    console.log("Speed increased");
    this.bgSpeed = this.bgSpeed * this.speedIncrement;
    this.playerBaseStats.baseVelocity =
      this.playerBaseStats.baseVelocity * this.speedIncrement;
    this.enemyBaseStats.baseVelocity =
      this.enemyBaseStats.baseVelocity * this.speedIncrement;
    this.enemyBaseStats.generateMaxSpeed =
      this.enemyBaseStats.generateMaxSpeed / this.speedIncrement;
    if (
      this.enemyBaseStats.generateMaxSpeed <
      this.enemyBaseStats.generateMaxSpeedLimit
    ) {
      this.enemyBaseStats.generateMaxSpeed = this.enemyBaseStats.generateMaxSpeedLimit;
    }
    if (
      this.enemyBaseStats.generateMinSpeed <
      this.enemyBaseStats.generateMinSpeedLimit
    ) {
      this.enemyBaseStats.generateMinSpeed = this.enemyBaseStats.generateMinSpeedLimit;
    }
  }
  createBg(texture) {
    let tiling = new PIXI.TilingSprite(
      texture,
      this.viewWidth,
      this.viewHeight
    );
    tiling.position.set(0, 0);
    this.app.stage.addChild(tiling);

    return tiling;
  }
  createObject(texture, width, height, x, y) {
    let sprite = new PIXI.Sprite(texture);
    sprite.position.set(x, y);
    sprite.height = height;
    sprite.width = width;
    this.app.stage.addChild(sprite);

    return sprite;
  }
  createScore() {
    let message = new PIXI.Text(`Score: ${this.score}`, { fill: "white" });
    message.position.set(10, 10);
    this.app.stage.addChild(message);

    return message;
  }
  generateRandomInt(max, min = 0) {
    let result = Math.floor(Math.random() * Math.floor(max));
    if (result < min) {
      result = result + min;
    }
    return result;
  }
  keyboard(value) {
    let key = {};
    key.value = value;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    //The `downHandler`
    key.downHandler = (event) => {
      if (event.key === key.value) {
        if (key.isUp && key.press) key.press();
        key.isDown = true;
        key.isUp = false;
        event.preventDefault();
      }
    };

    //The `upHandler`
    key.upHandler = (event) => {
      if (event.key === key.value) {
        if (key.isDown && key.release) key.release();
        key.isDown = false;
        key.isUp = true;
        event.preventDefault();
      }
    };

    //Attach event listeners
    const downListener = key.downHandler.bind(key);
    const upListener = key.upHandler.bind(key);

    window.addEventListener("keydown", downListener, false);
    window.addEventListener("keyup", upListener, false);

    // Detach event listeners
    key.unsubscribe = () => {
      window.removeEventListener("keydown", downListener);
      window.removeEventListener("keyup", upListener);
    };

    return key;
  }
  gameLoop(delta) {
    this.updateBg();
    this.updatePlayerPosition();
    this.updateEnemiesPosition();
  }
  updatePlayerPosition() {
    // jumping
    if (this.playerBaseStats.state === PLAYER_STATES.JUMPING) {
      if (this.player.y > this.playerBaseStats.boundaryY) {
        this.player.y = this.player.y - this.playerBaseStats.baseVelocity;
      } else {
        this.playerBaseStats.state = PLAYER_STATES.FALLING;
      }
    }
    // falling
    if (this.playerBaseStats.state === PLAYER_STATES.FALLING) {
      if (this.player.y < this.playerBaseStats.baseY) {
        this.player.y = this.player.y + this.playerBaseStats.baseVelocity;
      } else {
        this.player.y = this.playerBaseStats.baseY;
        this.playerBaseStats.state = PLAYER_STATES.NORMAL;
      }
    }
  }
  updateEnemiesPosition() {
    for (let i = 0; i < this.enemies.length; i++) {
      this.enemies[i].x = this.enemies[i].x - this.enemyBaseStats.baseVelocity;
      if (this.enemies[i].x <= 0) {
        this.enemies[i].destroy();
        this.enemies.splice(i, 1);
      }
      if (
        this.playerBaseStats.state !== PLAYER_STATES.DIED &&
        this.isCollided(this.player, this.enemies[i])
      ) {
        this.playerDie();
      }
    }
  }
  isCollided(a, b) {
    let aBox = a.getBounds();
    if (b === undefined) {
      return false;
    }
    let bBox = b.getBounds();
    return (
      aBox.x + aBox.width - this.collisionGap > bBox.x &&
      aBox.x < bBox.x + bBox.width + this.collisionGap &&
      aBox.y + aBox.height - this.collisionGap > bBox.y &&
      aBox.y < bBox.y + bBox.height + this.collisionGap
    );
  }
  playerDie() {
    if (this.playerBaseStats.state !== PLAYER_STATES.DIED) {
      this.enemyBaseStats.baseVelocity = 0;
      this.playerBaseStats.baseVelocity = 0;
      this.bgSpeed = 0;
      this.playerBaseStats.state = PLAYER_STATES.DIED;
      this.setState({
        dialogOpen: true,
      });
    }
  }
  updateBg() {
    this.bgX = this.bgX - this.bgSpeed;
    this.bgBack.tilePosition.x = this.bgX;
    this.score = Math.floor(this.score + this.bgSpeed * 10);
    this.pixiScore.text = `Score: ${this.score}`;
  }
  handleCloseSnackbar() {
    this.setState({
      open: false,
      message: "",
    });
  }
  handleCloseDialog() {
    this.setState({
      dialogOpen: false,
    });
  }
  handleReload() {
    window.location.reload(false);
  }
  render() {
    return (
      <div>
        <Snackbar
          open={this.state.open}
          autoHideDuration={5000}
          onClose={this.handleCloseSnackbar}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
        >
          <Alert
            variant="filled"
            onClose={this.handleCloseSnackbar}
            severity={this.state.severity}
          >
            {this.state.message}
          </Alert>
        </Snackbar>
        <Dialog
          open={this.state.dialogOpen}
          onClose={this.handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"You died!!!"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Do you want to play again?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color="primary">
              It's enough
            </Button>
            <Button onClick={this.handleReload} color="primary" autoFocus>
              Sure
            </Button>
          </DialogActions>
        </Dialog>
        <div
          className=""
          ref={(el) => {
            this.gameZone = el;
          }}
        />
      </div>
    );
  }
}

export default Tankathon;
