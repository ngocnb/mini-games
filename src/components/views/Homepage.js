import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import tileData from "../../fixture/homepageTileData";
import withWidth, { isWidthUp } from "@material-ui/core/withWidth";
import { NamedLink } from "../router";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    // width: 500,
    // height: 450,
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileData = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */
class Homepage extends React.Component {
  getGridListCols() {
    if (isWidthUp("xl", this.props.width)) {
      return 5;
    }

    if (isWidthUp("lg", this.props.width)) {
      return 4;
    }

    if (isWidthUp("md", this.props.width)) {
      return 3;
    }

    if (isWidthUp("sm", this.props.width)) {
      return 2;
    }

    return 1;
  }
  classes() {
    useStyles();
  }
  render() {
    return (
      <div className={"Homepage"}>
        <GridList
          cellHeight={"auto"}
          className={this.classes.gridList}
          cols={this.getGridListCols()}
        >
          {tileData.map((tile, index) => (
            <GridListTile key={index}>
              <NamedLink to={tile.route}>
                <img src={tile.img} alt={tile.title} className={"tile-image"} />
              </NamedLink>
              <GridListTileBar
                title={tile.title}
                subtitle={<span>by: {tile.author}</span>}
                actionIcon={
                  <IconButton
                    aria-label={`info about ${tile.title}`}
                    className={this.classes.icon}
                  >
                    <InfoIcon />
                  </IconButton>
                }
              />
            </GridListTile>
          ))}
        </GridList>
      </div>
    );
  }
}

export default withWidth()(Homepage);
